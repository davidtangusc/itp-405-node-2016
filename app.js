require('dotenv').config();

var express = require('express');
var bodyParser = require('body-parser');
var apiVersionCheck = require('./middleware/api-version-check');
var app = express();
var Song = require('./models/song');
var Artist = require('./models/artist');
var Promise = require('bluebird');

app.use(bodyParser.json()); // for parsing application/json
app.use(apiVersionCheck);

app.get('/api/v2/artists/:id', function(req, res) {
  Artist.findById(req.params.id)
    .then(checkIfArtistExists)
    .then(findSongsForArtist)
    .then(sendResponse)
    .catch(sendErrorResponse);

  function sendErrorResponse() {
    return res.json({
      error: 'Artist not found'
    });
  }

  function checkIfArtistExists(artist) {
    if (!artist) {
      throw new Error('Artist not found');
    }

    return artist;
  }

  function findSongsForArtist(artist) {
    var songsPromise = Song.findAll({
      where: {
        artist_id: artist.id
      }
    });

    return Promise.all([ artist, songsPromise ]);
  }

  function sendResponse(data) {
    res.json({
      artist: data[0],
      songs: data[1]
    });
  }
});

app.get('/api/v2/songs', function(req, res) {
  var promise = Song.findAll({
    // where: {
    //   title: {
    //     like: '%' + req.query.title + '%'
    //   }
    // },
    order: 'playCount DESC'
  }); // fulfilled, rejected, pending

  promise.then(function(songs) {
    res.json(songs);
  }, function() {
    console.log('there was an error');
  });
});

app.post('/api/v2/toys', function(req, res) {
  var body = req.body;
  console.log(body);
  res.json(req.body);
});

app.get('/api/v2/toys/:id', function(req, res) {
  var theID = req.params.id;
  var queryParams = req.query;

  console.log(queryParams);

  res.json({
    data: {
      id: theID
    }
  });
});

app.get('/api/v2/toys', function(req, res) {
  res.json({
    data: {}
  });
});



app.listen(8000, function() {
  console.log('Listening on port 8000');
});
