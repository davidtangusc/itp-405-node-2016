module.exports = function(req, res, next) {
  if (req.url.indexOf('api/v1') > -1) {
    res.json({
      message: 'v1 of the API is deprecated. Please use v2.'
    });
  } else {
    next();
  }
};
