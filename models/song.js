var sequelize = require('./../config/sequelize');
var Sequelize = require('sequelize');

var Song = sequelize.define('song', {
  title: {
    type: Sequelize.STRING,
    field: 'title'
  },
  playCount: {
    type: Sequelize.INTEGER,
    field: 'play_count'
  }
}, {
  timestamps: false
});

module.exports = Song;
