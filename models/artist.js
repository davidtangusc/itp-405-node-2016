var sequelize = require('./../config/sequelize');
var Sequelize = require('sequelize');

module.exports = sequelize.define('artist', {
  name: {
    type: Sequelize.STRING,
    field: 'artist_name'
  }
}, {
  timestamps: false
});
